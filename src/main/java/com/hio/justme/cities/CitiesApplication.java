package com.hio.justme.cities;

import com.hio.justme.cities.management.main.ExtraCode;
import com.hio.justme.cities.testClasses.TestAdder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class CitiesApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CitiesApplication.class, args);
		ExtraCode extraCode = new ExtraCode(ctx);
		extraCode.mainBranch();
	}

}
