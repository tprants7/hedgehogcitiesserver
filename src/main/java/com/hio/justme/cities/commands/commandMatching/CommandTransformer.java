package com.hio.justme.cities.commands.commandMatching;

import com.hio.justme.cities.commands.basic.AgentMovement;
import com.hio.justme.cities.commands.basic.WorkSaver;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.LandSquareService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CommandTransformer {
    private final AgentService agentService;
    private final LandSquareService landSquareService;
    private final WorkIdMatcher workIdMatcher;

    public AgentMovement makeMovementCommand(Long charId, Long squareId) {
        AgentMovement newMovement = new AgentMovement();
        newMovement.setAgent(agentService.getAgentById(charId));
        newMovement.setDestination(landSquareService.getOneLandSquare(squareId));
        return newMovement;
    }

    public WorkSaver makeWorkSaver(Long charId, WorkCommand workCommand) {
        return makeWorkSaver(charId, workCommand.getWorkId());
    }

    public WorkSaver makeWorkSaver(Long charId, int workId) {
        return new WorkSaver(
                agentService.getAgentById(charId),
                workIdMatcher.findWorkNameWithId(workId));
    }
}
