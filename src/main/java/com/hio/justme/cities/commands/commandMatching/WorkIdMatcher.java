package com.hio.justme.cities.commands.commandMatching;

import com.hio.justme.cities.enums.WorkName;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class WorkIdMatcher {


    public WorkName findWorkNameWithId(int id) {
        for(WorkName oneWork : WorkName.values()) {
            if(oneWork.workId == id) {
                return oneWork;
            }
        }
        return null;
    }
}
