package com.hio.justme.cities.commands.commandMatching;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.management.resource.ResourceManager;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.LandSquareService;
import com.hio.justme.cities.service.SettlementLvlService;
import com.hio.justme.cities.service.SettlementService;
import com.hio.justme.cities.service.factions.ControlBarService;
import com.hio.justme.cities.service.factions.ControlValueService;
import com.hio.justme.cities.service.factions.FactionService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@RequiredArgsConstructor
public class ServiceDirectory {
    private final AgentService agentService;
    private final SettlementService settlementService;
    private final LandSquareService landSquareService;
    private final ControlValueService controlValueService;
    private final ControlBarService controlBarService;
    private final FactionService factionService;
    private final ResourceManager resourceManager;

    public boolean agentOnSettlement(Agent agent) {
        if(agent.getLocation().getSettlement() != null) {
            return true;
        }
        return false;
    }

}
