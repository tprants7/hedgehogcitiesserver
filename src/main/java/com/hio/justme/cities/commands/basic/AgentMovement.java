package com.hio.justme.cities.commands.basic;

import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.squares.LandSquare;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@AllArgsConstructor
@RequiredArgsConstructor
public class AgentMovement {
    private Agent agent;
    private LandSquare destination;

    public void doMovement(ServiceDirectory MCRS) {
        this.agent.setLocation(this.destination);
        this.agent.setCurrentWork(null);
        MCRS.getAgentService().editOneAgent(this.agent);
    }
}
