package com.hio.justme.cities.commands.basic;

import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.enums.WorkName;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WorkSaver {
    private Agent agent;
    private WorkName workName;

    public boolean checkRequirements(ServiceDirectory MCRS) {
        if(!onSettlement(MCRS)) {
            return false;
        }
        return true;
    }

    private boolean onSettlement(ServiceDirectory MCRS) {
        return MCRS.agentOnSettlement(agent);
    }

    public boolean doAction(ServiceDirectory MCRS) {
        if(!this.checkRequirements(MCRS)) {
            return false;
        }
        else {
            changeJobOnChar(MCRS);
            return true;
        }
    }

    private void changeJobOnChar(ServiceDirectory MCRS) {
        this.agent.setCurrentWork(this.workName);
        MCRS.getAgentService().editOneAgent(this.agent);
    }
}
