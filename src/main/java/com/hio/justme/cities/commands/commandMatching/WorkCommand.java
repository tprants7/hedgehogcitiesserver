package com.hio.justme.cities.commands.commandMatching;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkCommand {
    //private long characterId;
    private long landSquareId;
    private int workId;
}
