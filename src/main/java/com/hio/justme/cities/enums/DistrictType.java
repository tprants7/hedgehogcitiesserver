package com.hio.justme.cities.enums;

public enum DistrictType {
    Agriculture, Logging, Mining
}
