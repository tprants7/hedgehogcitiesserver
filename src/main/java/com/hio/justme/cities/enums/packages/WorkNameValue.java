package com.hio.justme.cities.enums.packages;

import com.hio.justme.cities.enums.WorkName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkNameValue {
    private WorkName workName;
    private int id;
}
