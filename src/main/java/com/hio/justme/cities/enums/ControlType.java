package com.hio.justme.cities.enums;

public enum ControlType {
    STATIC, DYNAMIC
}
