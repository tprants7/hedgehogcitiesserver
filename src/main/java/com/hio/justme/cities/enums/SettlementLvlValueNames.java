package com.hio.justme.cities.enums;

public enum SettlementLvlValueNames {
    Village, Town, City, Metropolis
}
