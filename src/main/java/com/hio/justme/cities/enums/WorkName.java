package com.hio.justme.cities.enums;

public enum WorkName {
    TributeWood(1),
    TributeFood(2),
    TributeStone(3),
    TributeGold(4);

    public int workId;

    WorkName(int workId){
        this.workId = workId;
    }
}
