package com.hio.justme.cities.enums;

public enum MaterialName {
    Wood, Gold, Stone, Food
}
