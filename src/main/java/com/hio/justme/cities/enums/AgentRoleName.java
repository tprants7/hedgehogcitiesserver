package com.hio.justme.cities.enums;

public enum AgentRoleName {
    Envoy, Warrior
}
