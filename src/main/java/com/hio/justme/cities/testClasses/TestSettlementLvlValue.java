package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.entity.SettlementLvlValue;
import com.hio.justme.cities.enums.SettlementLvlValueNames;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
public class TestSettlementLvlValue {

    public List<SettlementLvlValue> generateTestValues() {
        List<SettlementLvlValue> values = new ArrayList<SettlementLvlValue>();
        int counter = 1;
        for(SettlementLvlValueNames oneName : SettlementLvlValueNames.values()) {
            values.add(new SettlementLvlValue(counter, oneName));
            counter++;
        }
        return values;
    }
}
