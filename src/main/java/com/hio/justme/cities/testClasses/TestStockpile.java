package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.MaterialName;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@NoArgsConstructor
public class TestStockpile {
    private List<StockpileElement> stockpile;

    public void makeTestData() {
        stockpile = new ArrayList<>();
        stockpile.add(new StockpileElement(MaterialName.Wood, 50));
        stockpile.add(new StockpileElement(MaterialName.Food, 100));
        stockpile.add(new StockpileElement(MaterialName.Stone, 20));
        stockpile.add(new StockpileElement(MaterialName.Gold, 100));
    }

    public List<StockpileElement> getAListOfTestElements() {
        List<StockpileElement> newStockpile = new ArrayList<>();
        newStockpile.add(new StockpileElement(MaterialName.Wood, 50));
        newStockpile.add(new StockpileElement(MaterialName.Food, 100));
        newStockpile.add(new StockpileElement(MaterialName.Stone, 20));
        newStockpile.add(new StockpileElement(MaterialName.Gold, 100));
        return newStockpile;
    }
}
