package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.entity.Settlement;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@NoArgsConstructor
public class TestSettlements {
    private List<Settlement> testSettlements;

    /*public TestSettlements() {
        this.testSettlements = new ArrayList<Settlement>();
        makeTestData();
    }*/

    public void makeTestData() {
        this.testSettlements = new ArrayList<Settlement>();
        this.testSettlements.add(new Settlement("Test city", 3));
        this.testSettlements.add(new Settlement("Test village", 1));
        this.testSettlements.add(new Settlement("Test town", 2));
    }

}
