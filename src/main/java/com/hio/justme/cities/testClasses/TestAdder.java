package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.SettlementLvlValue;
import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.map.MapGenerator;
import com.hio.justme.cities.service.*;
import com.hio.justme.cities.service.factions.ControlBarService;
import com.hio.justme.cities.service.factions.FactionService;
import com.hio.justme.cities.management.influence.InfluenceManagementService;
import com.hio.justme.cities.service.materials.FactionStockpileService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@RequiredArgsConstructor
public class TestAdder {
    private final TestSettlements testSettlements;
    private final SettlementService settlementService;
    //private final TestSquares testSquares;
    private final MapGenerator mapGenerator;
    private final LandSquareService landSquareService;
    private final DistrictService districtService;
    private final TestSettlementLvlValue testSettlementLvlValue;
    private final SettlementLvlService settlementLvlService;
    private final TestAgents testAgents;
    private final AgentService agentService;
    private final TestStockpile testStockpile;
    private final FactionStockpileService factionStockpileService;
    private final ControlBarService controlBarService;
    private final TestFactions testFactions;
    private final FactionService factionService;
    private final ServiceDirectory serviceDirectory;
    private final InfluenceManagementService influenceManagementService;

    public void addAllTestData() {
        List<Faction> factions = this.testFactions.getTestFactions();

        for(Faction oneFaction : factions) {
            this.factionService.addNewFaction(oneFaction);
        }

        factions = this.factionService.getAllFactions();

        List<SettlementLvlValue> settlementLvlValues = this.getSettlementLvlValues();

        for(SettlementLvlValue oneValue : settlementLvlValues) {
            this.settlementLvlService.addNewSettlementLvlValue(oneValue);
        }

        if(testSettlements.getTestSettlements() == null) {
            testSettlements.makeTestData();
        }

        List<LandSquare> newMap = this.mapGenerator.makeAMap(10, 10);

        for(LandSquare oneSquare : newMap) {
            this.landSquareService.addSquare(oneSquare);
        }

        for(Settlement oneSettlement : testSettlements.getTestSettlements()) {
            this.settlementService.addSettlement(oneSettlement);
        }

        this.testAgents.generateValues(factions.get(0));
        for(Agent oneAgent : this.testAgents.getAgentList()) {
            this.agentService.addNewAgent(oneAgent);
        }

        this.testStockpile.makeTestData();
        for(Faction oneFaction : factions) {
            FactionStockpile newStockpile = new FactionStockpile(oneFaction);
            newStockpile.getElements().addAll(this.testStockpile.getAListOfTestElements());
            this.factionStockpileService.AddNewFactionStockpile(newStockpile);
        }
        /*for(StockpileElement oneElement : this.testStockpile.getStockpile()) {
            this.stockpileElementService.addNewStockpileElement(oneElement);
        }*/

        makingConnections();
        makingDistricts();
        makingControlBars();
        //this.setCapital();
        settingLocationsForAgents();
    }

    public void makingConnections() {
        List<Settlement> settlementList = this.settlementService.getSettlements();
        List<LandSquare> landSquareList = this.landSquareService.getLandSquares();
        landSquareList.get(1).setSettlement(settlementList.get(0));
        landSquareList.get(3).setSettlement(settlementList.get(1));
        landSquareList.get(5).setSettlement(settlementList.get(2));
        this.landSquareService.editOneLandSquare(landSquareList.get(1));
        this.landSquareService.editOneLandSquare(landSquareList.get(3));
        this.landSquareService.editOneLandSquare(landSquareList.get(5));
    }

    public void settingLocationsForAgents() {
        List<LandSquare> landSquareList = this.landSquareService.getLandSquares();
        List<Agent> agentList = this.agentService.getAllAgents();
        agentList.get(0).setLocation(landSquareList.get(1));
        this.agentService.editOneAgent(agentList.get(0));
        agentList.get(1).setLocation(landSquareList.get(1));
        this.agentService.editOneAgent(agentList.get(1));
        agentList.get(2).setLocation(landSquareList.get(1));
        this.agentService.editOneAgent(agentList.get(2));

    }

    public void makingDistricts() {
        List<Settlement> settlementList = this.settlementService.getSettlements();
        for(Settlement oneSettlement : settlementList) {
            oneSettlement.makeDistricts();
            this.districtService.addDistrictsFromSettlement(oneSettlement);
            this.settlementService.editOneSettlement(oneSettlement);
        }
    }

    public void makingControlBars() {
        List<Settlement> settlementList = this.settlementService.getSettlements();
        for(Settlement oneSettlement : settlementList) {
            oneSettlement.makeControlBar();
            //oneSettlement.addDynamicControlValue(new ControlValue(this.testFactions.getTestFactions().get(0).getId(), 50, ControlType.DYNAMIC));
            this.controlBarService.addNewBar(oneSettlement.getControlDetails());
            this.settlementService.editOneSettlement(oneSettlement);
        }
        settlementList = this.settlementService.getSettlements();
        this.influenceManagementService.addCapitalToFaction(this.testFactions.getTestFactions().get(0), settlementList.get(0));
        /*for(Settlement oneSettlement : settlementList) {
            Long barId = oneSettlement.getControlDetails().getId();
            ControlValue newValue = new ControlValue(this.testFactions.getTestFactions().get(0), 50, ControlType.DYNAMIC);
            this.controlBarService.addNewControlValueToBar(barId, newValue);
            //this.settlementService.editOneSettlement(oneSettlement);
        }*/

    }

    /*public void setCapital() {
        Settlement newCapital = this.settlementService.getSettlements().get(0);
        Faction playerFaction = this.factionService.getAllFactions().get(0);
        playerFaction.assignFirstTimeCapital(newCapital.getId(), this.serviceDirectory);

    }*/

    public List<SettlementLvlValue> getSettlementLvlValues() {
        return this.testSettlementLvlValue.generateTestValues();
    }
}
