package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.entity.factions.Faction;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
@Data
public class TestFactions {
    private List<Faction> testFactions = new ArrayList<>();

    @PostConstruct
    public void makeTestFactions() {
        this.testFactions.add(new Faction("Player"));
        this.testFactions.add(new Faction("Tester"));
    }
}
