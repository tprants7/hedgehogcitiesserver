package com.hio.justme.cities.testClasses;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.enums.AgentRoleName;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@NoArgsConstructor
public class TestAgents {
    private List<Agent> agentList;

    public void generateValues(Faction agentFaction) {
        this.agentList = new ArrayList<>();
        this.agentList.add(new Agent("Alex", AgentRoleName.Envoy, agentFaction));
        this.agentList.add(new Agent("Naomi", AgentRoleName.Envoy, agentFaction));
        this.agentList.add(new Agent("Amos", AgentRoleName.Warrior, agentFaction));
    }
}
