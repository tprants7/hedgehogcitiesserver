package com.hio.justme.cities.map;

import com.hio.justme.cities.entity.squares.LandSquare;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
@Component
public class MapGenerator {

    public List<LandSquare> makeAMap(int height, int length) {
        List<LandSquare> newMap = new ArrayList<LandSquare>();
        for(int x = 0; x < length; x++) {
            for(int y = 0; y < height; y++) {
                newMap.add(new LandSquare(terrainChooser(), x, y));
            }
        }
        return  newMap;
    }

    private TerrainName terrainChooser() {
        int amount = TerrainName.values().length;
        int randomValue = new Random().nextInt(amount);
        TerrainName chosenName = TerrainName.values()[randomValue];
        return chosenName;
    }
}
