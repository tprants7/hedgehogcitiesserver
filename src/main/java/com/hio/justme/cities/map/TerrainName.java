package com.hio.justme.cities.map;

public enum TerrainName {
    FOREST, PLAIN, HILLS, MOUNTAINS, SWAMP
}
