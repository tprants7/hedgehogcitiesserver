package com.hio.justme.cities.repository.materials.factionStockpile;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.repository.graphAccess.BaseRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FactionStockpileRepository extends JpaRepository<FactionStockpile, Long>,
        BaseRepository<FactionStockpile, Long> {
    @EntityGraph(value = "FactionStockpile-elements")
    FactionStockpile findByFaction(Faction faction);
}
