package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.Building;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuildingRepository extends JpaRepository<Building, Long> {
}
