package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.squares.LandSquare;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgentRepository extends JpaRepository<Agent, Long> {
    List<Agent> findByLocation(LandSquare location);
}
