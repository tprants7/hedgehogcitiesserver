package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.DistrictGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictGroupRepository extends JpaRepository<DistrictGroup, Long> {
}
