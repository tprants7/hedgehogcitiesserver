package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.BuildingTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuildingTemplateRepository extends JpaRepository<BuildingTemplate, Long> {

}
