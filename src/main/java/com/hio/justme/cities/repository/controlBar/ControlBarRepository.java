package com.hio.justme.cities.repository.controlBar;

import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.repository.graphAccess.BaseRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ControlBarRepository extends JpaRepository<ControlBar, Long>, BaseRepository<ControlBar, Long> {
}
