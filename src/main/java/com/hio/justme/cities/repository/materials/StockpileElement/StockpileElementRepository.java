package com.hio.justme.cities.repository.materials.StockpileElement;

import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.MaterialName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockpileElementRepository extends JpaRepository<StockpileElement, Long> {
    List<StockpileElement> findByName(MaterialName materialName);
}
