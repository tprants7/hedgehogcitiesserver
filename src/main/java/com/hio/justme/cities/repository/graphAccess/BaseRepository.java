package com.hio.justme.cities.repository.graphAccess;

public interface BaseRepository<D, T> {
    D findWithGraph(T id, String graphName);
}
