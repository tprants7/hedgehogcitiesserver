package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.Settlement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LandSquareRepository extends JpaRepository<LandSquare, Long> {
    List<LandSquare> findBySettlement(Settlement settlement);
}
