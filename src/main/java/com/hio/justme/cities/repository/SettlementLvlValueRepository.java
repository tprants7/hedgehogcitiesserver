package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.SettlementLvlValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SettlementLvlValueRepository extends JpaRepository<SettlementLvlValue, Long> {
    List<SettlementLvlValue> findByLvl(int lvl);
}
