package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.BuildingProject;
import com.hio.justme.cities.entity.Settlement;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuildingProjectRepository extends JpaRepository<BuildingProject, Long> {
    List<BuildingProject> findByLocation(Settlement location);
}
