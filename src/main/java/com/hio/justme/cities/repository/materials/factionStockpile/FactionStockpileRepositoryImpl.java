package com.hio.justme.cities.repository.materials.factionStockpile;

import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.repository.graphAccess.BaseRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FactionStockpileRepositoryImpl implements BaseRepository<FactionStockpile, Long>  {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public FactionStockpile findWithGraph(Long id, String graphName) {
        EntityGraph entityGraph = entityManager.getEntityGraph(graphName);
        Map<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.fetchgraph", entityGraph);
        return entityManager.find(FactionStockpile.class, id, properties);
    }
}
