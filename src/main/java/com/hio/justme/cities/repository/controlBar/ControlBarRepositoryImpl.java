package com.hio.justme.cities.repository.controlBar;

import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.repository.graphAccess.BaseRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ControlBarRepositoryImpl implements BaseRepository<ControlBar, Long> {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ControlBar findWithGraph(Long id, String graphName) {
        EntityGraph entityGraph = entityManager.getEntityGraph(graphName);
        Map<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.fetchgraph", entityGraph);
        return entityManager.find(ControlBar.class, id, properties);
    }
}
