package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.factions.ControlValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ControlValueRepository extends JpaRepository<ControlValue,Long> {
}
