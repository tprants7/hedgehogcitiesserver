package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.factions.Faction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FactionRepository extends JpaRepository<Faction, Long> {

}
