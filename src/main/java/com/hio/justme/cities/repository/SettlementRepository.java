package com.hio.justme.cities.repository;

import com.hio.justme.cities.entity.Settlement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettlementRepository extends JpaRepository<Settlement, Long> {
}
