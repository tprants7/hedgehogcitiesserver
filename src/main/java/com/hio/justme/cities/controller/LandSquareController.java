package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.LandSquareService;
import com.hio.justme.cities.service.SettlementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/landSquares")
@RequiredArgsConstructor
public class LandSquareController {
    private final LandSquareService landSquareService;
    private final SettlementService settlementService;
    private final AgentService agentService;

    @GetMapping
    public List<LandSquare> getLandSquares() {
        return landSquareService.getLandSquares();
    }

    @GetMapping("/{id}")
    public LandSquare getLandSquare(@PathVariable Long id) {
        return landSquareService.getOneLandSquare(id);
    }

    @GetMapping("/{id}/agents")
    public List<Agent> getAgentsOnSquare(@PathVariable Long id) {
        LandSquare location = this.landSquareService.getOneLandSquare(id);
        return this.agentService.getAllAgentsOnLocation(location);
    }

    @GetMapping("/settlement/{id}")
    public LandSquare getLandSquareBySettlementId(@PathVariable Long id) {
        Settlement settlement = this.settlementService.getSettlement(id);
        List<LandSquare> squares = this.landSquareService.getSettlementLocation(settlement);
        return squares.get(0);
    }
}
