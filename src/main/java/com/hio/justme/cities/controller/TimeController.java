package com.hio.justme.cities.controller;

import com.hio.justme.cities.management.time.TurnPackage;
import com.hio.justme.cities.service.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/turn")
@RequiredArgsConstructor
public class TimeController {
    private final TimeService timeService;

    @GetMapping
    public int getTurn() {
        return this.timeService.getCurrentTurn();
    }
    @GetMapping("/schedule")
    public int getScheduledTime() {
        return this.timeService.getScheduledTime();
    }

    @GetMapping("/full")
    public TurnPackage getTurnPackage() {
        return this.timeService.getTurnPackage();
    }

    @PostMapping("/pause")
    public boolean setPause() {
        return this.timeService.togglePause();
    }
}
