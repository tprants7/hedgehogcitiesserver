package com.hio.justme.cities.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hio.justme.cities.entity.serializers.LandSquareIdSerializer;
import com.hio.justme.cities.entity.serializers.WorkNameEnumSerializer;
import com.hio.justme.cities.enums.WorkName;
import com.hio.justme.cities.enums.packages.WorkNameValue;
import com.hio.justme.cities.service.CommandNameService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/CommandName")
@RequiredArgsConstructor
public class CommandNameController {
    private final CommandNameService commandNameService;

    @GetMapping
    public List<WorkNameValue> getAllCommands() {
        return commandNameService.getListOfWorkNames();
    }
}
