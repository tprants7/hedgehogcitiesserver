package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.DistrictGroup;
import com.hio.justme.cities.entity.StartCostSet;
import com.hio.justme.cities.service.DistrictService;
import com.hio.justme.cities.service.building.BuildProjectSetUpService;
import com.hio.justme.cities.service.building.ProjectStartCostService;
import com.hio.justme.cities.service.SettlementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/projectCost")
@RequiredArgsConstructor
public class ProjectCostSetController {
    private final ProjectStartCostService projectStartCostService;
    private final DistrictService districtService;
    private final BuildProjectSetUpService buildProjectSetUpService;

    @GetMapping("/onSettlement/{id}")
    public List<StartCostSet> getAllStartCosts(@PathVariable Long id) {
        return this.projectStartCostService.getAllStartCostSets();
    }

    @GetMapping("/forDistrict/{id}")
    public StartCostSet getDistrictBuildCost(@PathVariable Long id) {
        DistrictGroup districtGroup = this.districtService.findDistrictGroupById(id);
        return this.projectStartCostService.getStartCostSetForType(districtGroup.getType());
    }

    @GetMapping("/forDistrict/{id}/canAfford")
    public boolean canAfford(@PathVariable long id) {
        return this.buildProjectSetUpService.hasResourcesToStart(
                this.projectStartCostService.getStartCostSetForType(
                        this.districtService.findDistrictGroupById(id).getType()));
    }
}
