package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.SettlementLvlValue;
import com.hio.justme.cities.service.SettlementLvlService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/settlementLvl")
@RequiredArgsConstructor
public class SettlementLvlValueController {
    private final SettlementLvlService settlementLvlService;

    @GetMapping("/{lvl}")
    public SettlementLvlValue getSettlementValueForLvl(@PathVariable int lvl) {
        List<SettlementLvlValue> values = this.settlementLvlService.getByLvl(lvl);
        return values.get(0);
    }
}
