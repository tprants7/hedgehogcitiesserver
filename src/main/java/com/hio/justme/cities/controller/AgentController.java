package com.hio.justme.cities.controller;

import com.hio.justme.cities.commands.basic.AgentMovement;
import com.hio.justme.cities.commands.basic.WorkSaver;
import com.hio.justme.cities.commands.commandMatching.CommandTransformer;
import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.StartCostSet;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.building.BuildProjectSetUpService;
import com.hio.justme.cities.service.building.ProjectStartCostService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/agent")
@RequiredArgsConstructor
public class AgentController {
    private final AgentService agentService;
    private final CommandTransformer commandTransformer;
    private final ServiceDirectory MCRS;
    private final ProjectStartCostService projectStartCostService;
    private final BuildProjectSetUpService buildProjectSetUpService;

    @GetMapping
    public List<Agent> getAllAgents() {
        return agentService.getAllAgents();
    }

    @GetMapping("/{id}")
    public Agent getAgentById(@PathVariable Long id) {
        return agentService.getAgentById(id);
    }

    @PostMapping("/{id}/move")
    public boolean moveAgent(@PathVariable Long id, @RequestBody Long squareId) {
        AgentMovement newMovement= this.commandTransformer.makeMovementCommand(id, squareId);
        newMovement.doMovement(MCRS);
        return true;
    }

    @PostMapping("/{agentId}/build")
    public boolean startProject(@PathVariable Long agentId, @RequestBody Long projectPlanId) {
        StartCostSet selectedProject = this.projectStartCostService.getStartCostSetForId(projectPlanId);
        Agent agent = this.agentService.getAgentById(agentId);
        return this.buildProjectSetUpService.startProject(agent, selectedProject);
    }

    @PostMapping("/{id}/command")
    public boolean giveNewCommand(@PathVariable Long id, @RequestBody int workId) {
        WorkSaver newWorkSaver = this.commandTransformer.makeWorkSaver(id, workId);
        return newWorkSaver.doAction(MCRS);
    }
}
