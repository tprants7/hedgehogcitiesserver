package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.service.materials.FactionStockpileService;
import com.hio.justme.cities.service.materials.StockpileElementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stockpile")
@RequiredArgsConstructor
public class StockpileController {
    private final StockpileElementService stockpileElementService;
    private final FactionStockpileService factionStockpileService;

    @GetMapping
    public List<StockpileElement> getAllElements() {
        return stockpileElementService.getFullStockpile();
    }

    @GetMapping("/{id}")
    public StockpileElement getElementById( @PathVariable Long id) {
        return stockpileElementService.getElementById(id);
    }

    @GetMapping("/faction")
    public List<FactionStockpile> getAllFactionStockpiles() {
        return factionStockpileService.getAllStockpiles();
    }

    @GetMapping("/faction/{id}")
    public FactionStockpile getFactionStockpileById( @PathVariable Long id) {
        return factionStockpileService.getOneStockpile(id);
    }
}
