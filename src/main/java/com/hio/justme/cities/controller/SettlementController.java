package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.BuildingProject;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.service.SettlementService;
import com.hio.justme.cities.service.building.BuildingProjectService;
import com.hio.justme.cities.testClasses.TestAdder;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/settlements")
@RequiredArgsConstructor
public class SettlementController {
    private final SettlementService settlementService;
    private final BuildingProjectService buildingProjectService;

    @GetMapping
    public List<Settlement> getSettlements() {
        return settlementService.getSettlements();
    }

    @GetMapping("/{id}")
    public Settlement getSettlement(@PathVariable Long id) {
        return settlementService.getSettlement(id);
    }

    @GetMapping("/{id}/buildingProjects")
    public List<BuildingProject> getBuildingProjects(@PathVariable Long id) {
        return this.buildingProjectService.getProjectsByLocation(this.settlementService.getSettlement(id));
    }

    /*@PostMapping
    public Long addSettlement(@RequestBody Settlement settlement) {
        return settlementService.addSettlement(settlement);
    }*/
}
