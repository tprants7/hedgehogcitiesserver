package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.squares.FieldOfSquares;
import com.hio.justme.cities.service.FieldSquareService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/field")
@RequiredArgsConstructor
public class FieldSquareController {
    private final FieldSquareService fieldSquareService;

    @GetMapping
    public FieldOfSquares getFieldOfSquares() {
        return this.fieldSquareService.getFieldOfSquares();
    }

    /*@GetMapping
    public LineOfSquares getLineOfSquares() {
        return this.fieldSquareService.getLineOfSquares();
    }*/
}
