package com.hio.justme.cities.controller;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.service.factions.FactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/factions")
@RequiredArgsConstructor
public class FactionController {
    private final FactionService factionService;

    @GetMapping
    public List<Faction> getAllFactions() {
        return this.factionService.getAllFactions();
    }

    @GetMapping("/{id}")
    public Faction getFactionById(@PathVariable Long id) {
        return this.factionService.findFactionById(id);
    }
}
