package com.hio.justme.cities.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hio.justme.cities.entity.serializers.LandSquareIdSerializer;
import com.hio.justme.cities.entity.serializers.SettlementIdSerializer;
import com.hio.justme.cities.enums.DistrictType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class BuildingProject {
    @Id
    @GeneratedValue
    private Long id;
    @JsonSerialize(using = SettlementIdSerializer.class)
    @ManyToOne
    private Settlement location;
    private DistrictType buildTarget;
    private int progress;
    private int finishMark;

    public BuildingProject(Settlement location, DistrictType target, int finishMark) {
        this.location = location;
        this.buildTarget = target;
        this.progress = 0;
        this.finishMark = finishMark;
    }

    public boolean isFinished() {
        return finishMark <= progress;
    }

    public void addProgress(int newProgress) {
        this.progress = this.progress + newProgress;
    }
}
