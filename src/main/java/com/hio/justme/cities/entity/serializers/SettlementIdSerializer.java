package com.hio.justme.cities.entity.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.hio.justme.cities.entity.Settlement;

import java.io.IOException;

public class SettlementIdSerializer extends StdSerializer<Settlement> {

    public SettlementIdSerializer() {
        this(null);
    }

    public SettlementIdSerializer(Class<Settlement> t) {
        super(t);
    }

    @Override
    public void serialize(Settlement settlement, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeNumber(settlement.getId());

    }
}
