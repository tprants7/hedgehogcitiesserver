package com.hio.justme.cities.entity;

import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.entity.factions.ControlValue;
import com.hio.justme.cities.enums.DistrictType;
import com.hio.justme.cities.service.factions.ControlBarService;
import com.hio.justme.cities.service.factions.ControlValueService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Settlement {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private int lvl;
    @OneToMany(fetch = FetchType.EAGER)
    private List<DistrictGroup> districts = new ArrayList<>();
    @OneToOne
    private ControlBar controlDetails;
    private int wealthCeiling;
    private int wealth;
    private int wealthGrowth;

    public Settlement(String name, int lvl) {
        this.name = name;
        this.lvl = lvl;
        this.wealth = 0;
        this.wealthGrowth = 10;
        this.wealthCeiling = this.calculateWealthCeailing();
    }

    public void makeDistricts() {
        //this.districts = new ArrayList<DistrictGroup>();
        this.districts.add(new DistrictGroup(DistrictType.Agriculture, 5));
        this.districts.add(new DistrictGroup(DistrictType.Logging, 5));
        this.districts.add(new DistrictGroup(DistrictType.Mining, 5));
        for(DistrictGroup oneGroup : this.districts) {
            oneGroup.addOneDistrict(this);
        }
    }

    public void makeControlBar() {
        this.controlDetails = new ControlBar(100);
    }

    public void addDynamicControlValue(ControlValue newValue) {
        this.controlDetails.getControlValues().add(newValue);
    }

    public void addStaticControlValue(ControlValue newValue, ServiceDirectory serviceDirectory) {
        ControlBar controlBar = serviceDirectory.getControlBarService().getBarById(this.controlDetails.getId());
        controlBar.getControlValues().add(newValue);
    }

    public int growWealth(){
        if(this.wealth < this.wealthCeiling) {
            this.wealth = this.wealth + this.wealthGrowth;
        }
        if(this.wealth > this.wealthCeiling) {
            this.wealth = this.wealthCeiling;
        }
        return this.wealth;
    }

    public int increaseWealthCeilingBy(int increase) {
        this.wealthCeiling = this.wealthCeiling + increase;
        return this.wealthCeiling;
    }

    public int decreaseWealthCeilingBy(int decrease) {
        return this.increaseWealthCeilingBy(-decrease);
    }

    public int increaseWealthGrowthBy(int increase) {
        this.wealthGrowth = this.wealthGrowth + increase;
        return this.wealthGrowth;
    }

    public int decreaseWealthGrowthBy(int decrease) {
        return this.increaseWealthGrowthBy(-decrease);
    }

    public DistrictGroup getDistrictGroupByType(DistrictType typeName) {
        for(DistrictGroup oneGroup : this.getDistricts()) {
            if(oneGroup.getType() == typeName) {
                return oneGroup;
            }
        }
        return null;
    }

    private int calculateWealthCeailing() {
        int calcCeiling = 0;
        calcCeiling = calcCeiling + this.getBaseWealthCeiling();
        for(DistrictGroup oneGroup : this.districts) {
            calcCeiling = calcCeiling + oneGroup.getDistrictGroupWealthContribution();
        }
        return calcCeiling;
    }

    private int getBaseWealthCeiling() {
        return 1000;
    }


}
