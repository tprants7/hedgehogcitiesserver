package com.hio.justme.cities.entity.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.hio.justme.cities.enums.WorkName;

import java.io.IOException;

public class WorkNameEnumSerializer extends StdSerializer<WorkName> {

    public WorkNameEnumSerializer() {
        this(null);
    }

    public WorkNameEnumSerializer(Class<WorkName> t) {
        super(t);
    }

    @Override
    public void serialize(WorkName workName, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("name", workName.name());
        gen.writeNumberField("id", workName.workId);
        gen.writeEndObject();
    }
}
