package com.hio.justme.cities.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.serializers.LandSquareIdSerializer;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.enums.AgentRoleName;
import com.hio.justme.cities.enums.WorkName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedEntityGraph(
        name = "Agent-faction",
        attributeNodes = {
                @NamedAttributeNode("faction")
        }
)
public class Agent {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private AgentRoleName role;
    @ManyToOne
    private Faction faction;
    @JsonSerialize(using = LandSquareIdSerializer.class)
    @ManyToOne
    private LandSquare location;
    private WorkName currentWork;

    public Agent(String name, AgentRoleName role, Faction faction){
        this.name = name;
        this.role = role;
        this.faction = faction;
    }
}
