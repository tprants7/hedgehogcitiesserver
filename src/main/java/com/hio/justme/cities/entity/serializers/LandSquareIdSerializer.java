package com.hio.justme.cities.entity.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.hio.justme.cities.entity.squares.LandSquare;

import java.io.IOException;

public class LandSquareIdSerializer extends StdSerializer<LandSquare> {

    public LandSquareIdSerializer() {
        this(null);
    }

    public LandSquareIdSerializer(Class<LandSquare> t) {
        super(t);
    }

    @Override
    public void serialize(LandSquare value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeNumber(value.getId());
        /*gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("terrainName", value.getTerrainName().name());
        gen.writeNumberField("xcoord", value.getXCoord());
        gen.writeNumberField("ycoord", value.getYCoord());
        gen.writeNumberField("settlementId", value.getSettlement().getId());
        gen.writeEndObject();*/
    }
}
