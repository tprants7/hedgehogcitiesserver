package com.hio.justme.cities.entity.squares;

import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.squares.LineOfSquares;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldOfSquares {
    private String name = "New Random";
    private List<LineOfSquares> lines = new ArrayList<LineOfSquares>();

    private void declareList() {
        this.lines = new ArrayList<LineOfSquares>();
    }

    public boolean hasList() {
        return this.lines != null;
    }

    public LineOfSquares getLineFor(int yCoord) {
        for(LineOfSquares oneLine : this.lines ) {
            if(oneLine.getYCoord() == yCoord) {
                return oneLine;
            }
        }
        return null;
    }

    private LineOfSquares makeNewLine(int yCoord) {
        LineOfSquares newLine = new LineOfSquares(yCoord);
        /*if(!hasList()) {
            this.declareList();
        }*/
        this.lines.add(newLine);
        return newLine;
    }

    public void addSquare(LandSquare newSquare) {
        int yCoord = newSquare.getYCoord();
        LineOfSquares designatedLine = this.getLineFor(yCoord);
        if(designatedLine == null) {
            designatedLine = makeNewLine(yCoord);
        }
        designatedLine.addSquare(newSquare);
    }

    public void addListOfSquares(List<LandSquare> squareList) {
        for(LandSquare oneSquare : squareList) {
            addSquare(oneSquare);
        }
    }

}
