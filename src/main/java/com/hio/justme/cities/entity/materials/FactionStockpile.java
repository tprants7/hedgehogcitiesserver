package com.hio.justme.cities.entity.materials;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.enums.MaterialName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@NamedEntityGraph(
        name = "FactionStockpile-elements",
        attributeNodes = {
                @NamedAttributeNode("elements")
        }
)
public class FactionStockpile {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    private Faction faction;
    @OneToMany
    private List<StockpileElement> elements = new ArrayList<>();

    public FactionStockpile(Faction faction) {
        this.faction = faction;
    }

    public boolean hasElementWithName(MaterialName materialName) {
        return this.getElementWithName(materialName) != null;
    }

    public StockpileElement getElementWithName(MaterialName materialName) {
        for(StockpileElement oneElement : this.elements) {
            if(oneElement.getName() == materialName) {
                return oneElement;
            }
        }
        return null;
    }
}
