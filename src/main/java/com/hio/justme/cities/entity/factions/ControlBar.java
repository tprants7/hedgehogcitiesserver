package com.hio.justme.cities.entity.factions;

import com.hio.justme.cities.enums.ControlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedEntityGraph(
        name = "ControlBar-ControlValue",
        attributeNodes = {
                @NamedAttributeNode(value = "controlValues", subgraph = "ControlValue-Faction"),
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "ControlValue-Faction",
                        attributeNodes = {
                                @NamedAttributeNode("faction")
                        }
                )
        }
)
public class ControlBar {
    @Id
    @GeneratedValue
    private Long id;
    private int maxDynamicPoints;
    @OneToMany
    private List<ControlValue> controlValues = new ArrayList<>();

    public ControlBar(int maxDynamicPoints) {
        this.maxDynamicPoints = maxDynamicPoints;
    }

    public int getAllClaimedPoints() {
        return this.getSumClaimedDynamicPoints();
    }

    public int getAllStaticPoints() {
        return this.getSumStaticPoints();
    }

    public int getMaxAllPoints() {
        return this.maxDynamicPoints + this.getSumStaticPoints();
    }

    private int getSumStaticPoints() {
        int totalValue = 0;
        for(ControlValue oneValue : this.controlValues) {
            if(oneValue.getControlType() == ControlType.STATIC) {
                totalValue = totalValue + oneValue.getControlPoints();
            }
        }
        return totalValue;
    }

    private int getSumClaimedDynamicPoints() {
        int totalValue = 0;
        for(ControlValue oneValue : this.controlValues) {
            if(oneValue.getControlType() == ControlType.DYNAMIC) {
                totalValue = totalValue + oneValue.getControlPoints();
            }
        }
        return totalValue;
    }

    public void increaseMaxDynamicBySavedValues() {
        this.maxDynamicPoints = this.getAllClaimedPoints();
    }

}
