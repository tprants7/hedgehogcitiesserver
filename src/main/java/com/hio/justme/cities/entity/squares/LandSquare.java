package com.hio.justme.cities.entity.squares;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.entity.serializers.LandSquareSerializer;
import com.hio.justme.cities.map.TerrainName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@JsonSerialize(using = LandSquareSerializer.class)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class LandSquare {
    @Id
    @GeneratedValue
    private long id;
    private TerrainName terrainName;
    private int xCoord;
    private int yCoord;
    @OneToOne
    @JoinColumn(name = "settlement_id", referencedColumnName = "id")
    private Settlement settlement;

    public LandSquare(TerrainName terrainName, int xCoord, int yCoord) {
        this.terrainName = terrainName;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }
}
