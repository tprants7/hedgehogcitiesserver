package com.hio.justme.cities.entity.squares;

import com.hio.justme.cities.entity.squares.LandSquare;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LineOfSquares {
    private int yCoord;
    private List<LandSquare> squares;

    public LineOfSquares(int yCoord) {
        this.yCoord = yCoord;
    }

    private void declareList() {
        this.squares = new ArrayList<LandSquare>();
    }

    private boolean hasList() {
        return this.squares != null;
    }

    public void addSquare(LandSquare newSquare) {
        if(!this.hasList()) {
            declareList();
        }
        this.squares.add(newSquare);
    }
}
