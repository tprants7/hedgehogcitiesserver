package com.hio.justme.cities.entity;

import com.hio.justme.cities.enums.DistrictType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class DistrictGroup {
    @Id
    @GeneratedValue
    private Long id;
    private DistrictType type;
    private int currentAmount;
    private int maxAmount;

    public DistrictGroup(DistrictType type, int maxAmount) {
        this.type = type;
        this.currentAmount = 0;
        this.maxAmount = maxAmount;
    }

    public int getOneDistrictWealthContribution() {
        return 200;
    }

    public int getDistrictGroupWealthContribution() {
        return this.currentAmount * this.getOneDistrictWealthContribution();
    }

    public void addOneDistrict(Settlement locationItsOn) {
        this.currentAmount++;
        locationItsOn.increaseWealthCeilingBy(this.getOneDistrictWealthContribution());
    }
}
