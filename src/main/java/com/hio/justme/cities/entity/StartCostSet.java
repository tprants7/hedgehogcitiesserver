package com.hio.justme.cities.entity;

import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.DistrictType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class StartCostSet {
    private Long id;
    private List<StockpileElement> elementCosts = new ArrayList<>();
    private DistrictType target;
    private int projectSize;

    public void addCostElement(StockpileElement newCost) {
        this.elementCosts.add(newCost);
    }
}
