package com.hio.justme.cities.entity.materials;

import com.hio.justme.cities.enums.MaterialName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class StockpileElement {
    @Id
    @GeneratedValue
    private Long id;
    private MaterialName name;
    private int amount;

    public StockpileElement(MaterialName name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public boolean nameMatches(StockpileElement otherElement) {
        return this.name == otherElement.getName();
    }

    public boolean addAmount(StockpileElement otherStack) {
        if(this.name != otherStack.getName()) {
            return false;
        }
        return this.summarizeAmount(otherStack.getAmount());
    }

    public boolean removeAmount(StockpileElement otherStack) {
        if(this.name != otherStack.getName()) {
            return false;
        }
        return this.summarizeAmount(-otherStack.getAmount());
    }

    private boolean summarizeAmount(int amount) {
        if(this.amount + amount >= 0) {
            this.amount = this.amount + amount;
            return true;
        }
        else {
            return false;
        }
    }
}
