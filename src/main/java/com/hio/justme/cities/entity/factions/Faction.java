package com.hio.justme.cities.entity.factions;

import com.hio.justme.cities.commands.commandMatching.ServiceDirectory;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.enums.ControlType;
import com.hio.justme.cities.service.SettlementService;
import com.hio.justme.cities.service.factions.FactionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Faction {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long capitalId;

    public Faction(String name) {
        this.name = name;
    }

    /*public void assignFirstTimeCapital(Long capitalId, ServiceDirectory serviceDirectory) {
        this.saveCapital(capitalId, serviceDirectory);
    }*/

    /*private void saveCapital(Long capitalId, ServiceDirectory serviceDirectory) {
        this.capitalId = capitalId;
        ControlValue newControlValue = new ControlValue(this.id, this.getCapitalPointAmount(), ControlType.STATIC);
        //newControlValue.setId(serviceDirectory.getControlValueService().addNewControlValue(newControlValue));
        Settlement capitalSettlement = serviceDirectory.getSettlementService().getSettlement(capitalId);
        capitalSettlement.addStaticControlValue(newControlValue, serviceDirectory);
        serviceDirectory.getControlBarService().editOneBar(capitalSettlement.getControlDetails());
        serviceDirectory.getFactionService().editExistingFaction(this);
    }*/

    /*private void removeCapital(ServiceDirectory serviceDirectory) {
        ControlValue foundValue = findCaptialStaticValue();
        if(foundValue == null) {
            System.out.println("couldn't find old capital control value");
            return;
        }
        this.capital.getControlDetails().getStaticValues().remove(foundValue);
        serviceDirectory.getControlBarService().editOneBar(this.capital.getControlDetails());
        serviceDirectory.getControlValueService().deleteControlValue(foundValue);

    }*/

    /*private ControlValue findCaptialStaticValue() {
        List<ControlValue> staticValues = this.capital.getControlDetails().getStaticValues();
        for(ControlValue oneValue : staticValues) {
            if(oneValue.getFaction() == this && oneValue.getControlPoints() == this.getCapitalPointAmount()) {
                return oneValue;
            }
        }
        return null;
    }*/

    private int getCapitalPointAmount() {
        return 50;
    }
}
