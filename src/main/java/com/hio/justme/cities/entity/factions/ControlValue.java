package com.hio.justme.cities.entity.factions;

import com.hio.justme.cities.enums.ControlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ControlValue {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Faction faction;
    private int controlPoints;
    private ControlType controlType;

    public ControlValue(Faction faction, int controlPoints, ControlType controlType) {
        this.faction = faction;
        this.controlPoints = controlPoints;
        this.controlType = controlType;
    }

    public int addPoints(int amount) {
        this.controlPoints = this.controlPoints + amount;
        return this.controlPoints;
    }

    public int removePoints(int amount) {
        return this.addPoints(-amount);
    }


}
