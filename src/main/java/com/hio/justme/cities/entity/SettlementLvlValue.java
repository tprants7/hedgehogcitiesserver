package com.hio.justme.cities.entity;

import com.hio.justme.cities.enums.SettlementLvlValueNames;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class SettlementLvlValue {
    @Id
    @GeneratedValue
    private Long id;
    private int lvl;
    private SettlementLvlValueNames valueName;

    public SettlementLvlValue(int lvl, SettlementLvlValueNames valueName) {
        this.lvl = lvl;
        this.valueName = valueName;
    }
}
