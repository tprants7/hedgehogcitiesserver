package com.hio.justme.cities.management.influence;

import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.entity.factions.ControlValue;
import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.enums.ControlType;
import com.hio.justme.cities.service.factions.ControlBarService;
import com.hio.justme.cities.service.factions.ControlValueService;
import com.hio.justme.cities.service.factions.FactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InfluenceManagementService {
    private final ControlBarService controlBarService;
    private final ControlValueService controlValueService;
    private final FactionService factionService;

    public void addCapitalToFaction(Faction faction, Settlement location) {
        if(faction.getCapitalId() == null) {
            this.addCapital(faction, location);
        }
        else {
            this.changeCapital(faction, location);
        }
    }

    private void addCapital(Faction faction, Settlement location) {
        faction.setCapitalId(location.getId());
        ControlBar changeBar = controlBarService.getBarByIdWithValues(location.getControlDetails().getId());
        changeBar.getControlValues().add(new ControlValue(faction, getCapitalPointAmount(), ControlType.STATIC));
        controlBarService.editOneBar(changeBar);
        factionService.editExistingFaction(faction);
    }

    private void addNewControlValueToSettlement(ControlBar changeBar, Faction faction, int value) {
        changeBar.getControlValues().add(new ControlValue(faction, value, ControlType.DYNAMIC));
        controlBarService.editOneBar(changeBar);
    }

    public void addNewDyanmicAmountToSettlement(Settlement location, Faction faction, int value) {
        ControlBar changeBar = controlBarService.getBarByIdWithValues(location.getControlDetails().getId());
        int saveAmount = this.takeLowerValue(value, getUnclaimedDynamicAmount(changeBar));
        ControlValue valueToChange = lookForDynamicValue(changeBar, faction);
        if(valueToChange == null) {
            addNewControlValueToSettlement(changeBar, faction, saveAmount);
        }
        else {
            valueToChange.addPoints(saveAmount);
            controlValueService.editExistingValue(valueToChange);
        }

    }

    private ControlValue lookForDynamicValue(ControlBar changeBar, Faction faction) {
        for(ControlValue oneValue : changeBar.getControlValues()) {
            if(oneValue.getControlType() == ControlType.DYNAMIC) {
                if(oneValue.getFaction().equals(faction)) {
                    return oneValue;
                }
            }
        }
        return null;
    }

    private int takeLowerValue(int one, int two) {
        if(one > two) {
            return two;
        }
        else {
            return one;
        }
    }

    private int getUnclaimedDynamicAmount(ControlBar changeBar) {
        return changeBar.getMaxDynamicPoints() - changeBar.getAllClaimedPoints();
    }

    private void changeCapital(Faction faction, Settlement location) {
        //Todo
    }

    private int getCapitalPointAmount() {
        return 50;
    }
}
