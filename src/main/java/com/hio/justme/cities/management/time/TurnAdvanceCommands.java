package com.hio.justme.cities.management.time;

import com.hio.justme.cities.service.turnAdvance.BuildProjectAdvanceService;
import com.hio.justme.cities.service.turnAdvance.InfluenceChangeService;
import com.hio.justme.cities.service.turnAdvance.JobFillService;
import com.hio.justme.cities.service.turnAdvance.SettlementGrowthService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@RequiredArgsConstructor
public class TurnAdvanceCommands {
    private final JobFillService jobFillService;
    private final SettlementGrowthService settlementGrowthService;
    private final BuildProjectAdvanceService buildProjectAdvanceService;
    private final InfluenceChangeService influenceChangeService;

    public void doTurnChangeCommands() {
        this.jobFillService.doJobs();
        this.settlementGrowthService.doGrowth();
        this.buildProjectAdvanceService.advanceProjects();
        this.influenceChangeService.doChangeCalc();
    }
}
