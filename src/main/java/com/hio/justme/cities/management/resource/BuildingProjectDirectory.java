package com.hio.justme.cities.management.resource;

import com.hio.justme.cities.entity.StartCostSet;
import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.DistrictType;
import com.hio.justme.cities.enums.MaterialName;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Component
public class BuildingProjectDirectory {
    private List<StartCostSet> projectCosts;

    @PostConstruct
    public void init() {
        this.projectCosts = new ArrayList<>();
        this.generateBuildingPlans();
    }

    private void generateBuildingPlans() {
        this.makePlanForAgriculture();
        this.makePlanForLogging();
        this.makePlanForMining();
    }

    private void makePlanForAgriculture() {
        StartCostSet agriCost = new StartCostSet();
        agriCost.setId((long) 1);
        agriCost.setProjectSize(100);
        agriCost.setTarget(DistrictType.Agriculture);
        agriCost.addCostElement(new StockpileElement(MaterialName.Gold, 100));
        agriCost.addCostElement(new StockpileElement(MaterialName.Stone, 50));
        this.projectCosts.add(agriCost);
    }

    private void makePlanForLogging() {
        StartCostSet loggingCost = new StartCostSet();
        loggingCost.setId((long) 2);
        loggingCost.setProjectSize(100);
        loggingCost.setTarget(DistrictType.Logging);
        loggingCost.addCostElement(new StockpileElement(MaterialName.Gold, 100));
        loggingCost.addCostElement(new StockpileElement(MaterialName.Food, 50));
        this.projectCosts.add(loggingCost);
    }

    private void makePlanForMining() {
        StartCostSet mineCost = new StartCostSet();
        mineCost.setId((long) 3);
        mineCost.setProjectSize(100);
        mineCost.setTarget(DistrictType.Mining);
        mineCost.addCostElement(new StockpileElement(MaterialName.Gold, 100));
        mineCost.addCostElement(new StockpileElement(MaterialName.Wood, 50));
        this.projectCosts.add(mineCost);
    }

    public StartCostSet findCostSetForDistrict(DistrictType type) {
        for(StartCostSet oneCostSet : this.projectCosts) {
            if(oneCostSet.getTarget() == type) {
                return oneCostSet;
            }
        }
        return null;
    }

    public StartCostSet findCostSetForId(long id) {
        for(StartCostSet oneCostSet : this.projectCosts) {
            if(oneCostSet.getId() == id) {
                return oneCostSet;
            }
        }
        return null;
    }
}
