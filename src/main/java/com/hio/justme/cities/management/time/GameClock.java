package com.hio.justme.cities.management.time;

import com.hio.justme.cities.service.turnAdvance.JobFillService;
import com.hio.justme.cities.service.turnAdvance.SettlementGrowthService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@RequiredArgsConstructor
public class GameClock {
    private int turnCycle = 60;
    private int currentMoment = 60;
    private int turn = 0;
    private boolean pause = false;
    private final TurnAdvanceCommands turnAdvanceCommands;

    /*public GameClock(int cycleLength) {
        this.turnCycle = cycleLength;
        this.currentMoment = cycleLength;
        this.turn = 0;
        this.pause = false;
    }*/

    public void advanceTime(){
        this.countDown();
    }

    private void countDown(){
        if(!pause){
            this.currentMoment--;
            if(this.currentMoment < 1) {
                counterAtZero();
            }
        }
    }

    private void counterAtZero(){
        this.currentMoment = this.turnCycle;
        this.advanceTurn();
    }

    private void advanceTurn() {
        this.turn++;
        this.turnAdvanceCommands.doTurnChangeCommands();
    }

    public boolean togglePause(){
        if(pause) {
            this.unPause();
        }
        else {
            this.pause();
        }
        return this.pause;
    }

    public void pause(){
        this.pause = true;
    }

    public void unPause(){
        this.pause = false;
    }
}
