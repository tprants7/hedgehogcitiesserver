package com.hio.justme.cities.management.time;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Timer;

@Component
@Data
public class WorldTime {
    private final GameClock gameClock;
    private Timer timer;
    private long delay = 1000;
    private long period = 1000;
    private TimeCounterTask timeCounterTask;

    public void advanceTime() {
        this.gameClock.advanceTime();
    }

    public void bootUpTimer() {
        this.timer = new Timer("Timer");
        this.timeCounterTask = new TimeCounterTask(this);
        this.timer.scheduleAtFixedRate(this.timeCounterTask, delay, period);
    }

    /*public long getScheduledTime() {
        return this.timeCounterTask.scheduledExecutionTime();
    }

    public int getScheduledSeconds() {
        LocalDateTime scheduleTime = LocalDateTime.ofEpochSecond(getScheduledTime(), 0, ZoneOffset.UTC);
        LocalDateTime currentTime = getCurrentTime();
        int secondsTill=  scheduleTime.getSecond() - currentTime.getSecond();
        System.out.println("current seconds: " + currentTime.getSecond());
        System.out.println("scheduled seconds: " + scheduleTime.getSecond());
        return secondsTill;
    }

    public LocalDateTime getCurrentTime() {
        LocalDateTime localTime = LocalDateTime.now();
        return localTime;
    }*/


}
