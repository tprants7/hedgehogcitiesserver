package com.hio.justme.cities.management.time;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TurnPackage {
    private int turn;
    private int timeTill;
    private boolean paused;
}
