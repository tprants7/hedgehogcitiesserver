package com.hio.justme.cities.management.resource;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.MaterialName;
import com.hio.justme.cities.service.materials.FactionStockpileService;
import com.hio.justme.cities.service.materials.StockpileElementService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ResourceManager {
    private final StockpileElementService stockpileElementService;
    private final FactionStockpileService factionStockpileService;

    public boolean addResource(MaterialName name, int amount, Faction faction) {
        return changeResourceValue(name, amount, faction);
    }

    public boolean removeResrouce(MaterialName name, int amount, Faction faction) {
        return changeResourceValue(name, -amount, faction);
    }

    private boolean changeResourceValue(MaterialName name, int amount, Faction faction) {
        FactionStockpile factionStockpile = factionStockpileService.getStockpileByFaction(faction);
        if(factionStockpile == null) {
            return false;
        }
        StockpileElement foundElement = factionStockpile.getElementWithName(name);
        if(foundElement == null) {
            StockpileElement newElement = new StockpileElement(name, amount);
            factionStockpile.getElements().add(newElement);
            factionStockpileService.editExistingStockpile(factionStockpile);
            //stockpileElementService.addNewStockpileElement(newElement);
        }
        else {
            foundElement.setAmount(foundElement.getAmount() + amount);
            stockpileElementService.editExistingElement(foundElement);
            /*StockpileElement foundElement = stockpileElementService.getElementByName(name).get(0);
            foundElement.setAmount(foundElement.getAmount() + amount);
            stockpileElementService.editExistingElement(foundElement);*/
        }
        return true;
    }
}
