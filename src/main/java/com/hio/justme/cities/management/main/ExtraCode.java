package com.hio.justme.cities.management.main;

import com.hio.justme.cities.management.time.WorldTime;
import com.hio.justme.cities.testClasses.TestAdder;
import org.springframework.context.ApplicationContext;

public class ExtraCode {
    private ApplicationContext ctx;

    public ExtraCode(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    public void mainBranch(){
        testBranch();
        counterBranch();
    }

    public void testBranch() {
        TestAdder testAdder = (TestAdder) ctx.getBean("testAdder");
        testAdder.addAllTestData();
    }

    public void counterBranch(){
        WorldTime worldTime = (WorldTime) ctx.getBean("worldTime");
        worldTime.bootUpTimer();
    }
}
