package com.hio.justme.cities.management.influence;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.entity.factions.ControlValue;
import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.enums.ControlType;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.LandSquareService;
import com.hio.justme.cities.service.factions.ControlBarService;
import com.hio.justme.cities.service.factions.ControlValueService;
import com.hio.justme.cities.service.factions.FactionService;
import com.hio.justme.cities.service.turnAdvance.InfluenceChangeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class SettlementInfluenceCalc {
    private final AgentService agentService;
    private final LandSquareService landSquareService;
    private final ControlValueService controlValueService;
    private final InfluenceManagementService influenceManagementService;
    private final FactionService factionService;


    public ControlBar calculateFirstStageInluence(Settlement settlement) {
        List<ControlValue> firstStageValues = this.makeFirstStageControlValues(getAllAgentsOnSettlement(settlement));
        ControlBar firstStageInfluence = new ControlBar(findSumOfPoints(firstStageValues));
        addAllControlValuesToBar(firstStageValues, firstStageInfluence);
        return firstStageInfluence;
    }

    private List<ControlValue> makeFirstStageControlValues(List<Agent> agents) {
        List<Faction> usedFactions = new ArrayList<>();
        List<ControlValue> controlValues = new ArrayList<>();
        for(Agent oneAgent : agents) {
            if(usedFactions.contains(oneAgent.getFaction())){
                controlValues.get(usedFactions.indexOf(oneAgent.getFaction())).addPoints(this.getOneAgentIdlePower());
            }
            else {
                usedFactions.add(oneAgent.getFaction());
                controlValues.add(new ControlValue(oneAgent.getFaction(), this.getOneAgentIdlePower(), ControlType.DYNAMIC));
            }
        }
        return controlValues;
    }

    private int findSumOfPoints(List<ControlValue> controlValues) {
        int sum = 0;
        for(ControlValue oneValue : controlValues) {
            sum = sum + oneValue.getControlPoints();
        }
        return sum;
    }

    private ControlBar addAllControlValuesToBar(List<ControlValue> controlValues, ControlBar controlBar) {
        for(ControlValue oneValue : controlValues) {
            controlBar.getControlValues().add(oneValue);
        }
        return controlBar;
    }

    private LandSquare getSettlementLocation(Settlement settlement) {
        return landSquareService.getSettlementLocation(settlement).get(0);
    }

    private List<Agent> getAllAgentsOnSettlement(Settlement settlement) {
        return agentService.getAllAgentsOnLocation(this.getSettlementLocation(settlement));
    }

    public ControlBar calculateSecondStageInfluence(Settlement settlement, ControlBar firstStageBar) {
        int maxDPOfSettlement = settlement.getControlDetails().getMaxDynamicPoints();
        int maxDPOfFirstStage = firstStageBar.getMaxDynamicPoints();
        //WARNING: there most likely will be rounding problems
        int pointMultiplier = maxDPOfSettlement / maxDPOfFirstStage;

        increaseFirstStagePoints(firstStageBar, pointMultiplier);

        debugCheck(firstStageBar, settlement);
        return firstStageBar;
    }

    private ControlBar increaseFirstStagePoints(ControlBar firstStageBar, int pointMultiplier) {
        for(ControlValue oneValue : firstStageBar.getControlValues()) {
            oneValue.setControlPoints(oneValue.getControlPoints() * pointMultiplier);
        }
        firstStageBar.increaseMaxDynamicBySavedValues();

        return firstStageBar;
    }

    private List<ControlValue> createPlusBars(ControlBar secondStageBar) {
        List<ControlValue> valueList = new ArrayList<>();
        for(ControlValue oneValue : secondStageBar.getControlValues()) {
            valueList.add(new ControlValue(oneValue.getFaction(), oneValue.getControlPoints(), ControlType.DYNAMIC ));
        }
        return valueList;
    }

    private List<ControlValue> addNegavtiveValues(List<ControlValue> plussBars, ControlBar settlementsCurrentValues) {
        ControlValue thisFactionValue;
        for(ControlValue oneValue : settlementsCurrentValues.getControlValues()) {
            thisFactionValue = findFactionValue(plussBars, oneValue.getFaction());
            if(thisFactionValue == null) {
                plussBars.add(new ControlValue(oneValue.getFaction(), -oneValue.getControlPoints(), ControlType.DYNAMIC));
            }
            else {
                thisFactionValue.setControlPoints(thisFactionValue.getControlPoints() - oneValue.getControlPoints());
            }
        }

        return plussBars;
    }

    private ControlValue findFactionValue(List<ControlValue> listOfValues, Faction faction) {
        for(ControlValue oneValue : listOfValues) {
            if(oneValue.getFaction().equals(faction)) {
                return oneValue;
            }
        }
        return null;
    }



    private void debugCheck(ControlBar firstStageBar, Settlement settlement) {
        if(firstStageBar.getMaxDynamicPoints() != settlement.getControlDetails().getMaxDynamicPoints()) {
            System.out.println(
                    "Dyanmic points dont match in second stage: " +
                            "settlement one: "+settlement.getControlDetails().getMaxDynamicPoints() +
                            ", calculated: " + firstStageBar.getMaxDynamicPoints());
        }
    }

    public void doTestInfluenceChange(Settlement settlement) {
        /*if(!this.getAllAgentsOnSettlement(settlement).isEmpty()) {
            Faction playerFaction = this.factionService.getAllFactions().get(0);
            addInfluenceAmount(settlement, playerFaction, 2);
        }*/
        changeInfluenceByAgent(settlement);
    }

    public void changeInfluenceByAgent(Settlement settlement) {
        List<ControlValue> firstValues = this.makeFirstStageControlValues(this.getAllAgentsOnSettlement(settlement));
        for(ControlValue oneValue : firstValues) {
            addInfluenceAmount(settlement, oneValue.getFaction(), oneValue.getControlPoints());
        }
    }

    private void addInfluenceAmount(Settlement settlement, Faction faction, int value) {
        this.influenceManagementService.addNewDyanmicAmountToSettlement(settlement, faction, value);
    }

    private int getOneAgentIdlePower() {
        return 10;
    }
}


