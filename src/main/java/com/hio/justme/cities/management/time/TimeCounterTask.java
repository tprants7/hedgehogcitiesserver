package com.hio.justme.cities.management.time;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.TimerTask;

@RequiredArgsConstructor
public class TimeCounterTask extends TimerTask {
    private final WorldTime worldTime;

    /*public TimeCounterTask(WorldTime worldTime) {
        this.worldTime = worldTime;
    }*/

    @Override
    public void run() {
        worldTime.advanceTime();
    }
}
