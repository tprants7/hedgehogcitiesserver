package com.hio.justme.cities.service.turnAdvance;

import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.service.SettlementService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class SettlementGrowthService {
    private final SettlementService settlementService;

    public void doGrowth() {
        List<Settlement> allSettlements = this.settlementService.getSettlements();
        for(Settlement oneSettlement : allSettlements) {
            oneSettlement.growWealth();
            this.settlementService.editOneSettlement(oneSettlement);
        }
    }
}
