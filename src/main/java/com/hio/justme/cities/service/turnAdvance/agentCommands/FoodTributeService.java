package com.hio.justme.cities.service.turnAdvance.agentCommands;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.DistrictGroup;
import com.hio.justme.cities.enums.DistrictType;
import com.hio.justme.cities.enums.MaterialName;
import com.hio.justme.cities.management.resource.ResourceManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodTributeService implements IAgentCommandService {
    private final ResourceManager resourceManager;

    @Override
    public boolean fillCommand(Agent agent) {
        DistrictGroup farming = agent.getLocation().getSettlement().getDistrictGroupByType(DistrictType.Agriculture);
        if(farming == null) {
            System.out.println("Wasn't able to find the district group");
            return false;
        }
        int resourceAmount = farming.getCurrentAmount() * 5;
        System.out.println("Found district group, resource amount: "+resourceAmount);
        this.resourceManager.addResource(MaterialName.Food, resourceAmount, agent.getFaction());
        return true;
    }
}
