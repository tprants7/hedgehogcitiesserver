package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.Building;
import com.hio.justme.cities.repository.BuildingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BuildingService {
    private final BuildingRepository buildingRepository;

    public Long addNewBuilding(Building newElement) {
        return this.buildingRepository.save(newElement).getId();
    }
}
