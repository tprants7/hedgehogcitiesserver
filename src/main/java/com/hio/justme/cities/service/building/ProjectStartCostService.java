package com.hio.justme.cities.service.building;

import com.hio.justme.cities.entity.StartCostSet;
import com.hio.justme.cities.enums.DistrictType;
import com.hio.justme.cities.management.resource.BuildingProjectDirectory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProjectStartCostService {
    private final BuildingProjectDirectory buildingProjectDirectory;

    public List<StartCostSet> getAllStartCostSets() {
        return this.buildingProjectDirectory.getProjectCosts();
    }

    public StartCostSet getStartCostSetForType(DistrictType type) {
        return this.buildingProjectDirectory.findCostSetForDistrict(type);
    }

    public StartCostSet getStartCostSetForId(Long id) {
        return this.buildingProjectDirectory.findCostSetForId(id);
    }

}
