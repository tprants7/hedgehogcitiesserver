package com.hio.justme.cities.service.materials;

import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.enums.MaterialName;
import com.hio.justme.cities.repository.materials.StockpileElement.StockpileElementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class StockpileElementService {
    private final StockpileElementRepository stockpileElementRepository;

    public List<StockpileElement> getFullStockpile() {
        return stockpileElementRepository.findAll();
    }

    public Long addNewStockpileElement(StockpileElement element) {
        return stockpileElementRepository.save(element).getId();
    }

    public StockpileElement editExistingElement(StockpileElement element) {
        return stockpileElementRepository.save(element);
    }

    public StockpileElement getElementById(Long id) {
        return stockpileElementRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Element with specific Id was not found"));
    }

    public List<StockpileElement> getElementByName(MaterialName name) {
        return stockpileElementRepository.findByName(name);
    }
}
