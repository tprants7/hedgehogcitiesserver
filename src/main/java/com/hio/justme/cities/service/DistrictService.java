package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.DistrictGroup;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.repository.DistrictGroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DistrictService {
    private final DistrictGroupRepository districtGroupRepository;

    public void addDistrictsFromSettlement(Settlement settlement) {
        List<DistrictGroup> newDistricts = settlement.getDistricts();
        for(DistrictGroup oneDistrictGroup : newDistricts) {
            this.districtGroupRepository.save(oneDistrictGroup);
        }
    }

    public DistrictGroup editDistrictGroup(DistrictGroup oneGroup) {
        return this.districtGroupRepository.save(oneGroup);
    }

    public DistrictGroup findDistrictGroupById(Long id) {
        return this.districtGroupRepository.findById(id).orElseThrow(
                () -> new RuntimeException("District group with specified id was not found"));
    }
}
