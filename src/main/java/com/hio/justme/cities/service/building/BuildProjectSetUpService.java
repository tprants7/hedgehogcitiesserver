package com.hio.justme.cities.service.building;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.BuildingProject;
import com.hio.justme.cities.entity.StartCostSet;
import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.service.materials.StockpileElementService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BuildProjectSetUpService {
    private final StockpileElementService stockpileElementService;
    private final BuildingProjectService buildingProjectService;

    public boolean startProject(Agent startAgent, StartCostSet projectTemplate) {
        if(!canStartProject(startAgent, projectTemplate)) {
            return false;
        }
        BuildingProject newProject = this.makeProjectElement(startAgent, projectTemplate);
        this.removeAllCostElements(projectTemplate.getElementCosts());
        buildingProjectService.addNewProject(newProject);
        return true;
    }

    private BuildingProject makeProjectElement(Agent startAgent, StartCostSet projectTemplate) {
        BuildingProject newProject = new BuildingProject(
                startAgent.getLocation().getSettlement(),
                projectTemplate.getTarget(),
                projectTemplate.getProjectSize());
        return newProject;
    }

    private void removeAllCostElements(List<StockpileElement> costs) {
        for(StockpileElement oneCost : costs) {
            this.removeOneResource(oneCost);
        }
    }

    private void removeOneResource(StockpileElement neededElement) {
        StockpileElement stockpileResource = this.findStockpileMatch(neededElement);
        if(stockpileResource == null) {
            return;
        }
        stockpileResource.removeAmount(neededElement);
        //stockpileResource.setAmount(stockpileResource.getAmount() - neededElement.getAmount());
        this.stockpileElementService.editExistingElement(stockpileResource);
    }

    public boolean canStartProject(Agent agent, StartCostSet startCostSet){
        if(!districtProjectCheck(agent, startCostSet)) {
            return false;
        }
        return true;
    }

    public boolean hasResourcesToStart(StartCostSet startCostSet) {
        return this.hasResources(startCostSet);
    }

    private boolean districtProjectCheck(Agent agent, StartCostSet startCostSet) {
        if(!agentIsOnSettlement(agent)) {
            return false;
        }
        if(!hasResources(startCostSet)) {
            return false;
        }
        return true;
    }

    private boolean agentIsOnSettlement(Agent agent) {
        if(agent.getLocation().getSettlement() == null) {
            return false;
        }
        return true;
    }

    private boolean hasResources(StartCostSet startCostSet) {
        List<StockpileElement> fullNeededList = startCostSet.getElementCosts();
        if(!checkArrayOfReqElements(fullNeededList)) {
            return false;
        }
        return true;
    }

    private boolean checkArrayOfReqElements(List<StockpileElement> neededElements) {
        for(StockpileElement oneElement : neededElements) {
            if(!checkOneStockpileReq(oneElement)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkOneStockpileReq(StockpileElement neededElement) {
        StockpileElement storedResource = this.findStockpileMatch(neededElement);
        if(storedResource == null) {
            return false;
        }
        if(storedResource.getAmount() < neededElement.getAmount()) {
            return false;
        }
        return true;
    }

    private StockpileElement findStockpileMatch(StockpileElement neededElement) {
        List<StockpileElement> storedResources = this.stockpileElementService.getElementByName(neededElement.getName());
        if(storedResources.isEmpty()) {
            return null;
        }
        else {
            return storedResources.get(0);
        }
    }
}
