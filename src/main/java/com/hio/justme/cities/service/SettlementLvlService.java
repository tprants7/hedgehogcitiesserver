package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.SettlementLvlValue;
import com.hio.justme.cities.repository.SettlementLvlValueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SettlementLvlService {
    private final SettlementLvlValueRepository settlementLvlValueRepository;

    public Long addNewSettlementLvlValue(SettlementLvlValue newElement) {
        return this.settlementLvlValueRepository.save(newElement).getId();
    }

    public SettlementLvlValue getById(Long id) {
        return this.settlementLvlValueRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Settlement lvl value with specified id was not found"));
    }

    public List<SettlementLvlValue> getByLvl(int lvl) {
        return this.settlementLvlValueRepository.findByLvl(lvl);
    }


}
