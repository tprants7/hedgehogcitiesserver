package com.hio.justme.cities.service.materials;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.entity.materials.FactionStockpile;
import com.hio.justme.cities.entity.materials.StockpileElement;
import com.hio.justme.cities.repository.materials.factionStockpile.FactionStockpileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class FactionStockpileService {
    private final FactionStockpileRepository factionStockpileRepository;
    private final StockpileElementService stockpileElementService;

    public List<FactionStockpile> getAllStockpiles() {
        return this.factionStockpileRepository.findAll();
    }

    public FactionStockpile getOneStockpile(Long id) {
        return factionStockpileRepository.findWithGraph(id, "FactionStockpile-elements");
    }

    public FactionStockpile getStockpileByFaction(Faction faction) {
        return factionStockpileRepository.findByFaction(faction);
    }

    public Long AddNewFactionStockpile(FactionStockpile factionStockpile) {
        for(StockpileElement oneElement : factionStockpile.getElements()) {
            stockpileElementService.addNewStockpileElement(oneElement);
        }
        return this.factionStockpileRepository.save(factionStockpile).getId();
    }

    public FactionStockpile editExistingStockpile(FactionStockpile factionStockpile) {
        return this.factionStockpileRepository.save(factionStockpile);
    }

    public boolean addNewStockpileElementToStockpile(FactionStockpile factionStockpile,
                                                     StockpileElement stockpileElement) {
        if(!factionStockpile.getElements().contains(stockpileElement)) {
            factionStockpile.getElements().add(stockpileElement);
        }
        this.stockpileElementService.addNewStockpileElement(stockpileElement);
        this.editExistingStockpile(factionStockpile);
        return true;
    }
}
