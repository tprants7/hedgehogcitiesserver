package com.hio.justme.cities.service;

import com.hio.justme.cities.management.time.TurnPackage;
import com.hio.justme.cities.management.time.WorldTime;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TimeService {
    private final WorldTime worldTime;

    public int getCurrentTurn() {
        return this.worldTime.getGameClock().getTurn();
    }

    public int getScheduledTime() {
        return this.worldTime.getGameClock().getCurrentMoment();
    }

    public TurnPackage getTurnPackage() {
        TurnPackage newPackage = new TurnPackage(
                this.worldTime.getGameClock().getTurn(),
                this.worldTime.getGameClock().getCurrentMoment(),
                this.worldTime.getGameClock().isPause());
        return newPackage;
    }

    public boolean togglePause() {
        return this.worldTime.getGameClock().togglePause();
    }
}
