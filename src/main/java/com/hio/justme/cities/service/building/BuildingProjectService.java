package com.hio.justme.cities.service.building;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.BuildingProject;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.repository.BuildingProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BuildingProjectService {
    private final BuildingProjectRepository buildingProjectRepository;

    public List<BuildingProject> getAllProjects() {
        return this.buildingProjectRepository.findAll();
    }

    public Long addNewProject(BuildingProject newBuildingProject) {
        return this.buildingProjectRepository.save(newBuildingProject).getId();
    }

    public BuildingProject editOneProject(BuildingProject changedProject) {
        return this.buildingProjectRepository.save(changedProject);
    }

    public List<BuildingProject> getProjectsByLocation(Settlement location) {
        return this.buildingProjectRepository.findByLocation(location);
    }

    public boolean deleteProject(BuildingProject projectToRemove) {
        this.buildingProjectRepository.delete(projectToRemove);
        return true;
    }
}
