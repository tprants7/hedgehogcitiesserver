package com.hio.justme.cities.service.factions;

import com.hio.justme.cities.entity.factions.Faction;
import com.hio.justme.cities.repository.FactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class FactionService {
    private final FactionRepository factionRepository;

    public Long addNewFaction(Faction newFaction) {
        return this.factionRepository.save(newFaction).getId();
    }

    public Faction editExistingFaction(Faction editedFaction) {
        return this.factionRepository.save(editedFaction);
    }

    public List<Faction> getAllFactions() {
        return this.factionRepository.findAll();
    }

    public Faction findFactionById(Long id) {
        return this.factionRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Faction with specified id was not found")
        );
    }
}
