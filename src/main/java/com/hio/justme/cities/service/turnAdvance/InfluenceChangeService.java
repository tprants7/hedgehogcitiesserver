package com.hio.justme.cities.service.turnAdvance;

import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.management.influence.SettlementInfluenceCalc;
import com.hio.justme.cities.service.SettlementService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class InfluenceChangeService {
    private final SettlementService settlementService;
    private final SettlementInfluenceCalc settlementInfluenceCalc;

    public void doChangeCalc() {
        List<Settlement> allSettlements = this.settlementService.getSettlements();
        for(Settlement oneSettlement : allSettlements) {
            settlementInfluenceCalc.doTestInfluenceChange(oneSettlement);
        }
    }
}
