package com.hio.justme.cities.service.factions;

import com.hio.justme.cities.entity.factions.ControlValue;
import com.hio.justme.cities.repository.ControlValueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ControlValueService {
    private final ControlValueRepository controlValueRepository;

    public List<ControlValue> getAllValues() {
        return this.controlValueRepository.findAll();
    }

    public ControlValue getValueById(Long id) {
        return this.controlValueRepository.findById(id).orElseThrow(
                () -> new RuntimeException("ControlValue with specified id was not found"));
    }

    public Long addNewControlValue(ControlValue newValue) {
        return this.controlValueRepository.save(newValue).getId();
    }

    public ControlValue editExistingValue(ControlValue editedValue) {
        return this.controlValueRepository.save(editedValue);
    }

    public void deleteControlValue(ControlValue oldValue) {
        this.controlValueRepository.delete(oldValue);
    }

}
