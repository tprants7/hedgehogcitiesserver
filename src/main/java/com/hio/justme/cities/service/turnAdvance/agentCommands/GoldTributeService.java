package com.hio.justme.cities.service.turnAdvance.agentCommands;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.enums.MaterialName;
import com.hio.justme.cities.management.resource.ResourceManager;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.SettlementService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class GoldTributeService implements IAgentCommandService{
    private final ResourceManager resourceManager;
    private final SettlementService settlementService;
    private final AgentService agentService;

    @Override
    public boolean fillCommand(Agent agent) {
        int resourceAmount = agent.getLocation().getSettlement().getWealth() / 10;
        this.resourceManager.addResource(MaterialName.Gold, resourceAmount, agent.getFaction());
        return true;
    }
}
