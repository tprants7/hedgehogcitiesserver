package com.hio.justme.cities.service.turnAdvance;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.enums.MaterialName;
import com.hio.justme.cities.enums.WorkName;
import com.hio.justme.cities.management.resource.ResourceManager;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.turnAdvance.agentCommands.FoodTributeService;
import com.hio.justme.cities.service.turnAdvance.agentCommands.GoldTributeService;
import com.hio.justme.cities.service.turnAdvance.agentCommands.StoneTributeService;
import com.hio.justme.cities.service.turnAdvance.agentCommands.WoodTributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class JobFillService {
    private final AgentService agentService;
    private final GoldTributeService goldTributeService;
    private final WoodTributeService woodTributeService;
    private final FoodTributeService foodTributeService;
    private final StoneTributeService stoneTributeService;

    public void doJobs(){
        List<Agent> agents = this.agentService.getAllAgents();
        for(Agent oneAgent : agents) {
            if(oneAgent.getCurrentWork() != null) {
                WorkName currentWork = oneAgent.getCurrentWork();
                if(currentWork == WorkName.TributeWood) {
                    this.woodTributeService.fillCommand(oneAgent);
                }
                if(currentWork == WorkName.TributeGold) {
                    this.goldTributeService.fillCommand(oneAgent);
                }
                if(currentWork == WorkName.TributeFood) {
                    this.foodTributeService.fillCommand(oneAgent);
                }
                if(currentWork == WorkName.TributeStone) {
                    this.stoneTributeService.fillCommand(oneAgent);
                }
            }
        }
    }
}
