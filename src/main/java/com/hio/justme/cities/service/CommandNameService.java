package com.hio.justme.cities.service;

import com.hio.justme.cities.enums.WorkName;
import com.hio.justme.cities.enums.packages.WorkNameValue;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CommandNameService {

    public List<WorkNameValue> getListOfWorkNames() {
        List<WorkNameValue> finalList = new ArrayList<WorkNameValue>();
        for(WorkName oneWork : WorkName.values()) {
            finalList.add(new WorkNameValue(oneWork, oneWork.workId));
        }
        return finalList;
    }
}
