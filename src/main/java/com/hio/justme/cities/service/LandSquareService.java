package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.repository.LandSquareRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class LandSquareService {
    private final LandSquareRepository landSquareRepository;

    public List<LandSquare> getLandSquares() {
        return landSquareRepository.findAll();
    }

    public LandSquare getOneLandSquare(Long id) {
        return landSquareRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Cand find square with this Id"));
    }

    public LandSquare editOneLandSquare(LandSquare oneSquare) {
        return landSquareRepository.save(oneSquare);
    }

    public Long addSquare(LandSquare newSquare) {
        return this.landSquareRepository.save(newSquare).getId();
    }

    public List<LandSquare> getSettlementLocation(Settlement settlement) {
        return this.landSquareRepository.findBySettlement(settlement);
    }

    /*public LandSquare getOneLandSquare(Settlement settlement) {
        return landSquareRepository.find
    }*/
}
