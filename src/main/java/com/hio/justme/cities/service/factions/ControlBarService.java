package com.hio.justme.cities.service.factions;

import com.hio.justme.cities.entity.factions.ControlBar;
import com.hio.justme.cities.entity.factions.ControlValue;
import com.hio.justme.cities.repository.controlBar.ControlBarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class ControlBarService {
    private final ControlBarRepository controlBarRepository;
    private final ControlValueService controlValueService;

    public List<ControlBar> getAllBars() {
        return this.controlBarRepository.findAll();
    }

    public ControlBar getBarById(Long id) {
        return this.controlBarRepository.findById(id).orElseThrow(
                () -> new RuntimeException("ControlBar with specified id was not found"));
    }

    public ControlBar getBarByIdWithValues(Long id) {
        return this.controlBarRepository.findWithGraph(id, "ControlBar-ControlValue");
    }

    public void addNewControlValueToBar(Long barId, ControlValue controlValue) {
        ControlBar bar = this.getBarByIdWithValues(barId);
        List<ControlValue> valueList = bar.getControlValues();
        valueList.add(controlValue);
        this.editOneBar(bar);
    }

    public Long addNewBar(ControlBar newBar) {
        this.updateListOfControlValues(newBar.getControlValues());
        this.updateListOfControlValues((newBar.getControlValues()));
        return this.controlBarRepository.save(newBar).getId();
    }

    private void updateListOfControlValues(List<ControlValue> values) {
        for(ControlValue oneValue : values) {
            this.controlValueService.editExistingValue(oneValue);
        }
    }

    public ControlBar editOneBar(ControlBar editedBar) {
        this.updateListOfControlValues(editedBar.getControlValues());
        this.updateListOfControlValues((editedBar.getControlValues()));
        return this.controlBarRepository.save(editedBar);
    }

    private void callForContent(ControlBar bar) {
        List<ControlValue> values = bar.getControlValues();
        values.size();
    }


}
