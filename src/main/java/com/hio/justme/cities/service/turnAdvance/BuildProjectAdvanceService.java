package com.hio.justme.cities.service.turnAdvance;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.BuildingProject;
import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.service.AgentService;
import com.hio.justme.cities.service.DistrictService;
import com.hio.justme.cities.service.LandSquareService;
import com.hio.justme.cities.service.SettlementService;
import com.hio.justme.cities.service.building.BuildingProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BuildProjectAdvanceService {
    private final SettlementService settlementService;
    private final BuildingProjectService buildingProjectService;
    private final AgentService agentService;
    private final LandSquareService landSquareService;
    private final DistrictService districtService;

    public void advanceProjects() {
        findProjectsToAdvance();

    }

    private void findProjectsToAdvance() {
        List<BuildingProject> projects = this.buildingProjectService.getAllProjects();
        for(BuildingProject oneProject : projects) {
            this.oneProjectAdvance(oneProject);
        }

    }

    private void oneProjectAdvance(BuildingProject oneProject) {
        Settlement location = oneProject.getLocation();
        List<LandSquare> squares = landSquareService.getSettlementLocation(location);
        List<Agent> agents = agentService.getAllAgentsOnLocation(squares.get(0));
        this.advanceOneprojectBy(oneProject, this.getProjectAdvanceNumber(agents));
    }

    private int getProjectAdvanceNumber(List<Agent> agentsOnSquare) {
        if(agentsOnSquare.isEmpty()) {
            return 0;
        }
        else {
            return 20;
        }
    }

    private void advanceOneprojectBy(BuildingProject oneProject, int advanceNr) {
        oneProject.addProgress(advanceNr);
        this.buildingProjectService.editOneProject(oneProject);
        if(oneProject.isFinished()) {
            makeBuildingFromProject(oneProject);
        }
    }

    private void makeBuildingFromProject(BuildingProject oneProject) {
        oneProject.getLocation().getDistrictGroupByType(oneProject.getBuildTarget()).addOneDistrict(oneProject.getLocation());
        this.settlementService.editOneSettlement(oneProject.getLocation());
        this.districtService.editDistrictGroup(oneProject.getLocation().getDistrictGroupByType(oneProject.getBuildTarget()));
        this.buildingProjectService.deleteProject(oneProject);
    }


}
