package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.squares.FieldOfSquares;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.entity.squares.LineOfSquares;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class FieldSquareService {
    private final LandSquareService landSquareService;

    public FieldOfSquares getFieldOfSquares() {
        FieldOfSquares newField = new FieldOfSquares();
        newField.addListOfSquares(this.landSquareService.getLandSquares());
        return newField;
    }

    public LineOfSquares getLineOfSquares() {
        LineOfSquares newLine = new LineOfSquares(1);
        List<LandSquare> squares = landSquareService.getLandSquares();
        for(LandSquare oneSquare : squares) {
            newLine.addSquare(oneSquare);
        }
        return newLine;
    }

}
