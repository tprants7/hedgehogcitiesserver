package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.BuildingTemplate;
import com.hio.justme.cities.repository.BuildingTemplateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BuildingTemplateService {
    private final BuildingTemplateRepository buildingTemplateRepository;

    public Long addBuildingTemplate(BuildingTemplate newElement) {
        return this.buildingTemplateRepository.save(newElement).getId();
    }
}
