package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.Settlement;
import com.hio.justme.cities.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;

    public List<Settlement> getSettlements() {
        return settlementRepository.findAll();
    }

    public Settlement getSettlement(Long id) {
        return settlementRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Settlement with specified id was not found"));
    }
    //public LandSquare editOneLandSquare(LandSquare oneSquare) {
    //        return landSquareRepository.save(oneSquare);
    //    }
    public Settlement editOneSettlement(Settlement oneSettlement) {
        return settlementRepository.save(oneSettlement);
    }

    public Long addSettlement(Settlement settlement) {
        return settlementRepository.save(settlement).getId();
    }
}
