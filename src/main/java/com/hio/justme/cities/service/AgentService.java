package com.hio.justme.cities.service;

import com.hio.justme.cities.entity.Agent;
import com.hio.justme.cities.entity.squares.LandSquare;
import com.hio.justme.cities.repository.AgentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AgentService {
    private final AgentRepository agentRepository;

    public List<Agent> getAllAgents() {
        return this.agentRepository.findAll();
    }

    public List<Agent> getAllAgentsOnLocation(LandSquare location) {
        return this.agentRepository.findByLocation(location);
    }

    public Agent getAgentById(Long id) {
        return this.agentRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Agent with specified id was not found"));
    }

    public Agent editOneAgent(Agent oneAgent) {
        return agentRepository.save(oneAgent);
    }

    public Long addNewAgent(Agent newAgent) {
        return this.agentRepository.save(newAgent).getId();
    }
}
