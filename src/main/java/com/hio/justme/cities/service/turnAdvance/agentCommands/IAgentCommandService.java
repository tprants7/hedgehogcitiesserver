package com.hio.justme.cities.service.turnAdvance.agentCommands;

import com.hio.justme.cities.entity.Agent;

public interface IAgentCommandService {

    public boolean fillCommand(Agent agent);
}
